import "./styles/style.scss";
import {
    generateRandomString,
    getAvailableSymbols,
    replaceSymbols,
} from "./helpers";

const N = prompt("Введите число N", "101");

// GET AVAILABLE SYMBOLS
const availableSymbols = getAvailableSymbols();

// GENERATE RANDOM STRING
const randomString = generateRandomString(availableSymbols, N);

alert(randomString);

// REPLACE LETTERS BY FIRST SYMBOL
const firstSymbol = prompt(
    "Введите символ, на который изменятся все буквы",
    "z"
);

const replacedString = replaceSymbols(firstSymbol, randomString, /[a-zA-Z]/gi);

alert(replacedString.string);

// REPLACE STRING BY SECOND LETTER
const secondSymbol = prompt(
    "Введите второй символ, на который изменятся все цифры",
    "B"
);

const replacedString2 = replaceSymbols(
    secondSymbol,
    replacedString.string,
    /[0-9]/gi
);

alert(replacedString2.string);

// COUNT REPEATS

const regExp = new RegExp(firstSymbol, "gi");
const regExp2 = new RegExp(secondSymbol, "gi");

const firstSymbolsCount = replacedString2.string.match(regExp).length;
const secondSymbolsCount = replacedString2.string.match(regExp2).length;

alert(`Количество измений на первый символ ${replacedString.count}`);
alert(`Количество изменний на второй символ ${replacedString2.count}`);
alert(`Всего первого символа ${firstSymbolsCount}`);
alert(`Всего второго символа ${secondSymbolsCount}`);
alert(
    `Количество символов которые остались не изменены ${
        +N - replacedString.count - replacedString2.count
    }`
);
