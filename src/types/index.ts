export interface IModifiedString {
    string: string;
    count: number;
}
