import { IModifiedString } from "./types";

export const getAvailableSymbols = (): Array<string> => {
    const unavailableSymbols = [67, 80, 83, 88];
    const numbers = Array.from({ length: 10 }, (_, i) =>
        String.fromCharCode(i + 48)
    );

    const upperCaseLetters = Array.from({ length: 26 }, (_, i) => {
        if (!unavailableSymbols.includes(i + 65))
            return String.fromCharCode(i + 65);
    }).filter((item) => item);

    const lowerCaseLetters = Array.from({ length: 26 }, (_, i) =>
        String.fromCharCode(i + 97)
    );

    const unionArray = numbers.concat(upperCaseLetters, lowerCaseLetters, [
        "+",
        "-",
        "_",
        "$",
        "~",
    ]);

    return unionArray;
};

export const generateRandomString = (
    availableSymbols: Array<string>,
    length: string
): string => {
    const generatedString: Array<string> = [];
    for (let index = 0; index < +length; index++) {
        generatedString.push(availableSymbols[getRandomInt(63)]);
    }
    return generatedString.join("");
};

const getRandomInt = (max: number): number => {
    return Math.floor(Math.random() * Math.floor(max));
};

// REPLACE FUNCTIONS

export const replaceSymbols = (
    symbol: string,
    string: string,
    regExp: RegExp
): IModifiedString => {
    const arrayFromStr = string.split("");
    let i = 0;
    const modifiedArray = arrayFromStr.map((item) => {
        if (item.match(regExp)) {
            i++;
            return symbol;
        }
        return item;
    });
    return { string: modifiedArray.join(""), count: i };
};
